package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.service.DamageListGoodsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author CYY
 * @date 2023-07-05 17:49
 */
@Api(tags = "商品报损")
@RestController
@RequestMapping("/damageListGoods")
public class DamageListGoodsController {

    @Autowired
    private DamageListGoodsService damageListGoodsService;

    /**
     * 保存商品报损信息
     * @param damageList  商品报损单信息实体
     * @param damageListGoodsStr 商品报损单信息JSON字符串
     * @return
     */
    @PostMapping("/save")
    public ServiceVO save(DamageList damageList,String damageListGoodsStr){
        return damageListGoodsService.save(damageList,damageListGoodsStr);
    }


    /**
     * 报损单查询
     * @param sTime  开始时间
     * @param eTime  结束时间
     * @return
     */
    @ApiOperation("报损单查询")
    @PostMapping("/list")
    public Map<String,Object> list(String sTime,String eTime){
        return damageListGoodsService.list(sTime,eTime);
    }

    /**
     * 查询报损单商品信息
     * @param damageListId  报损单Id
     * @return
     */
    @ApiOperation("查询报损单商品信息")
    @PostMapping("/goodsList")
    public Map<String,Object> goodsList(Integer damageListId){
        return damageListGoodsService.goodsList(damageListId);
    }

}
