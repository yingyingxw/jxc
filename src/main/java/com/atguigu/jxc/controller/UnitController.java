package com.atguigu.jxc.controller;

import com.atguigu.jxc.service.UnitService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**商品单位
 * @author CYY
 * @date 2023-07-04 18:21
 */
@Api(tags = "商品单位")
@RestController
@RequestMapping("/unit")
public class UnitController {

    @Autowired
    private UnitService unitService;

    /**
     * 查询所有商品单位
     * @return
     */
    @ApiOperation("查询所有商品单位")
    @PostMapping("/list")
    public Map<String,Object> getUnitList(){
        return unitService.list();
    }
}
