package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.service.OverflowListGoodsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author CYY
 * @date 2023-07-05 19:50
 */
@Api(tags = "商品报溢")
@RestController
@RequestMapping("/overflowListGoods")
public class OverflowListGoodsController {

    @Autowired
    private OverflowListGoodsService overflowListGoodsService;

    /**
     * 新增报溢单
     * @param overflowList   商品报溢单实体
     * @param overflowListGoodsStr  商品报溢单信息JSON字符串
     * @return
     */
    @ApiOperation("新增报溢单")
    @PostMapping("/save")
    public ServiceVO save(OverflowList overflowList,String overflowListGoodsStr){
        return overflowListGoodsService.save(overflowList,overflowListGoodsStr);

    }


    /**
     * 报溢单查询
     * @param sTime  开始时间
     * @param eTime  结束时间
     * @return
     */
    @ApiOperation("报溢单查询")
    @PostMapping("/list")
    public Map<String,Object> list(String sTime, String eTime){
        return overflowListGoodsService.list(sTime,eTime);
    }

    /**
     * 报溢单商品信息
     * @param overflowListId  报损单Id
     * @return
     */
    @ApiOperation("报溢单商品信息")
    @PostMapping("/goodsList")
    public Map<String,Object> goodsList(Integer overflowListId){
        return overflowListGoodsService.goodsList(overflowListId);
    }

}
