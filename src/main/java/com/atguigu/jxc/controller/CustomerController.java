package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.ws.Service;
import java.util.Map;

/**
 * @author CYY
 * @date 2023-07-04 15:34
 */
@Api(tags = "客户管理")
@RestController
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    /**
     * 分页查询客户列表
     * @param page  当前页数
     * @param rows  每页显示记录数
     * @param customerName  客户名
     * @return
     */
    @ApiOperation("客户列表分页（名称模糊查询）")
    @PostMapping("/list")
    public Map<String,Object> getCustomerByPage(Integer page, Integer rows,String customerName){
        return customerService.list(page,rows,customerName);
    }

    /**
     * 添加或者修改客户信息
     * @param customer
     * @return
     */
    @ApiOperation("客户添加或修改")
    @PostMapping("/save")
    public ServiceVO saveOrUpdateCustomer(Customer customer){
        return customerService.save(customer);
    }

    /**
     * 删除客户（支持批量删除）
     * @param ids
     * @return
     */
    @ApiOperation("客户删除（支持批量删除）")
    @PostMapping("/delete")
    public ServiceVO deleteCustomer(String ids){
        String[] idList = ids.split(",");
        return customerService.delete(idList);
    }

}
