package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**供应商管理
 * @author CYY
 * @date 2023-07-04 10:56
 */
@Api(tags = "供应商管理")
@RestController
@RequestMapping("/supplier")
public class SupplierController {

    @Autowired
    private SupplierService supplierService;

    /**
     * 分页查询供应商
     * @param page 当前页数
     * @param rows  每页显示的记录数
     * @param supplierName 供应商名称
     * @return
     */
    @ApiOperation("分页查询供应商")
    @PostMapping("/list")
    public Map<String,Object> getSupplierByPage( Integer page,Integer rows, String supplierName){
        return supplierService.list(page,rows,supplierName);
    }

    /**
     * 添加或者修改供应商信息
     * @param supplier 供应商信息实体
     * @return
     */
    @ApiOperation("供应商添加或修改")
    @PostMapping("/save")
    public ServiceVO saveOrUpdateSupplier(Supplier supplier){
        return supplierService.save(supplier);
    }

    /**
     * 删除供应商（支持批量删除）
     * @param ids
     * @return
     */
    @ApiOperation("删除供应商（支持批量删除）")
    @PostMapping("/delete")
    public ServiceVO deleteSupplier(String ids){
        String[] idList = ids.split(",");
        return supplierService.delete(idList);
    }

}
