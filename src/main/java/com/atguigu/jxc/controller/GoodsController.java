package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.service.GoodsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @description 商品信息Controller
 */
@Api(tags = "商品信息")
@RestController
@RequestMapping("/goods")
public class GoodsController {

    @Autowired
    private GoodsService goodsService;

    /**
     * 分页查询商品库存信息
     * 查询当前库存（可根据商品类别、商品编码或名称搜索）
     * @param page 当前页
     * @param rows 每页显示条数
     * @param codeOrName 商品编码或名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @ApiOperation("查询当前库存（可根据商品类别、商品编码或名称搜索）")
    @PostMapping("/listInventory")
    public Map<String,Object> listInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId){
        return goodsService.listInventory(page,rows,codeOrName,goodsTypeId);
    }


    /**
     * 分页查询商品信息（可以根据分类、名称查询）
     * @param page 当前页
     * @param rows 每页显示条数
     * @param goodsName 商品名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @ApiOperation("查询所有商品信息（可以根据分类、名称查询）")
    @PostMapping("/list")
    public Map<String,Object> getGoodsList(Integer page,Integer rows,String goodsName,Integer goodsTypeId){
        return goodsService.list(page,rows,goodsName,goodsTypeId);

    }


    /**
     * 生成商品编码
     * @return
     */
    @RequestMapping("/getCode")
    @RequiresPermissions(value = "商品管理")
    public ServiceVO getCode() {
        return goodsService.getCode();
    }

    /**
     * 添加或修改商品信息
     * @param goods 商品信息实体
     * @return
     */
    @ApiOperation("商品添加或修改")
    @PostMapping("/save")
    public ServiceVO saveGoods(Goods goods){
        return goodsService.save(goods);
    }

    /**
     * 删除商品信息
     * (要判断商品状态,入库、有进货和销售单据的不能删除)
     * @param goodsId 商品ID
     * @return
     */
    @ApiOperation("商品删除")
    @PostMapping("/delete")
    public ServiceVO deleteGoods(Integer goodsId){
        return goodsService.delete(goodsId);
    }

    /**
     * 分页查询无库存商品信息
     * 无库存商品列表展示（可以根据商品名称或编码查询）
     * @param page 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    @ApiOperation("无库存商品列表展示（可以根据商品名称或编码查询）")
    @PostMapping("/getNoInventoryQuantity")
    public Map<String,Object> getNoInventoryQuantity(Integer page,Integer rows,String nameOrCode){
        return goodsService.getNoInventoryQuantity(page,rows,nameOrCode);
    }


    /**
     * 分页查询有库存商品信息
     * 有库存商品列表展示（可以根据商品名称或编码查询）
     * @param page 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    @ApiOperation("有库存商品列表展示（可以根据商品名称或编码查询）")
    @PostMapping("/getHasInventoryQuantity")
    public Map<String,Object> getHasInventoryQuantity(Integer page,Integer rows,String nameOrCode) {
        return goodsService.getHasInventoryQuantity(page,rows,nameOrCode);
    }


    /**
     * 添加商品期初库存
     * @param goodsId 商品ID
     * @param inventoryQuantity 库存
     * @param purchasingPrice 成本价
     * @return
     */
    @ApiOperation("添加库存、修改数量或成本价")
    @PostMapping("/saveStock")
    public ServiceVO saveStock(Integer goodsId,Integer inventoryQuantity,double purchasingPrice){
        return goodsService.saveStock(goodsId,inventoryQuantity,purchasingPrice);
    }

    /**
     * 删除商品库存
     * （要判断商品状态 入库、有进货和销售单据的不能删除）
     * @param goodsId 商品ID
     * @return
     */
    @ApiOperation("删除库存")
    @PostMapping("/deleteStock")
    public ServiceVO deleteStock(Integer goodsId){
        return goodsService.deleteStock(goodsId);
    }

    /**
     * 查询库存报警商品信息
     *查询所有 当前库存量 小于 库存下限的商品信息
     * @return
     */
    @ApiOperation("查询库存报警商品信息")
    @PostMapping("/listAlarm")
    public Map<String,Object> listAlarm(){
        return goodsService.listAlarm();
    }

}
