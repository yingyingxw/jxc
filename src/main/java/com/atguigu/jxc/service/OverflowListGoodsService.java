package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.OverflowList;

import java.util.Map;

/**
 * @author CYY
 * @date 2023-07-05 19:52
 */
public interface OverflowListGoodsService {

    /**
     * 新增报溢单
     * @param overflowList   商品报溢单实体
     * @param overflowListGoodsStr  商品报溢单信息JSON字符串
     * @return
     */
    ServiceVO save(OverflowList overflowList, String overflowListGoodsStr);


    /**
     * 报溢单查询
     * @param sTime  开始时间
     * @param eTime  结束时间
     * @return
     */
    Map<String, Object> list(String sTime, String eTime);

    /**
     * 报溢单商品信息
     * @param overflowListId  报损单Id
     * @return
     */
    Map<String, Object> goodsList(Integer overflowListId);

}
