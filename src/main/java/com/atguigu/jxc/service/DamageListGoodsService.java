package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;

import java.util.Map;

/**
 * @author CYY
 * @date 2023-07-05 17:55
 */
public interface DamageListGoodsService {

    /**
     * 保存商品报损信息
     * @param damageList  商品报损单信息实体
     * @param damageListGoodsStr 商品报损单信息JSON字符串
     * @return
     */
    ServiceVO save(DamageList damageList, String damageListGoodsStr);

    /**
     * 报损单查询
     * @param sTime  开始时间
     * @param eTime  结束时间
     * @return
     */
    Map<String, Object> list(String sTime, String eTime);

    /**
     * 查询报损单商品信息
     * @param damageListId  报损单Id
     * @return
     */
    Map<String, Object> goodsList(Integer damageListId);

}
