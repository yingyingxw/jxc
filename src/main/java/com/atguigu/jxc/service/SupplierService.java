package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;

import java.util.Map;

/**
 * @author CYY
 * @date 2023-07-04 10:59
 */
public interface SupplierService {
    /**
     * 分页查询供应商
     * @param page
     * @param rows
     * @param supplierName
     * @return
     */
    Map<String, Object> list(Integer page, Integer rows, String supplierName);

    /**
     * 添加或者修改供应商信息
     * @param supplier 供应商信息实体
     * @return
     */
    ServiceVO save(Supplier supplier);


    /**
     * 删除供应商（支持批量删除）
     * @param idList
     * @return
     */
    ServiceVO delete(String[] idList);
}
