package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;

import java.util.ArrayList;

/**
 * @description
 */
public interface GoodsTypeService {
    ArrayList<Object> loadGoodsType();

    /**
     * 新增分类
     * @param goodsTypeName 商品类别名称
     * @param pId   父商品类别id
     * @return
     */
    ServiceVO save(String goodsTypeName, Integer pId);

    /**
     * 删除分类
     * @param goodsTypeId
     * @return
     */
    ServiceVO delete(Integer goodsTypeId);
}
