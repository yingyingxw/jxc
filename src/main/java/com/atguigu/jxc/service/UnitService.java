package com.atguigu.jxc.service;

import java.util.Map;

/**
 * @author CYY
 * @date 2023-07-04 18:58
 */
public interface UnitService {
    /**
     * 查询所有商品单位
     * @return
     */
    Map<String, Object> list();

}
