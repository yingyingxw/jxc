package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;

import java.util.Map;

public interface GoodsService {


    ServiceVO getCode();



    /**
     * 分页查询商品信息（可以根据分类、名称查询）
     * @param page 当前页
     * @param rows 每页显示条数
     * @param goodsName 商品名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    Map<String, Object> list(Integer page, Integer rows, String goodsName, Integer goodsTypeId);

    /**
     * 添加或修改商品信息
     * @param goods 商品信息实体
     * @return
     */
    ServiceVO save(Goods goods);

    /**
     * 删除商品信息
     * (要判断商品状态,入库、有进货和销售单据的不能删除)
     * @param goodsId 商品ID
     * @return
     */
    ServiceVO delete(Integer goodsId);

    /**
     * 分页查询商品库存信息
     * 查询当前库存（可根据商品类别、商品编码或名称搜索）
     * @param page 当前页
     * @param rows 每页显示条数
     * @param codeOrName 商品编码或名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    Map<String, Object> listInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId);

    /**
     * 分页查询无库存商品信息
     * 无库存商品列表展示（可以根据商品名称或编码查询）
     * @param page 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode);

    /**
     * 分页查询有库存商品信息
     * 有库存商品列表展示（可以根据商品名称或编码查询）
     * @param page 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode);

    /**
     * 添加商品期初库存
     * @param goodsId 商品ID
     * @param inventoryQuantity 库存
     * @param purchasingPrice 成本价
     * @return
     */
    ServiceVO saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice);

    /**
     * 删除商品库存
     * （要判断商品状态 入库、有进货和销售单据的不能删除）
     * @param goodsId 商品ID
     * @return
     */
    ServiceVO deleteStock(Integer goodsId);

    /**
     * 查询库存报警商品信息
     *查询所有 当前库存量 小于 库存下限的商品信息
     * @return
     */
    Map<String, Object> listAlarm();

}
