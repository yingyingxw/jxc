package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;

import java.util.Map;

/**
 * @author CYY
 * @date 2023-07-04 15:36
 */
public interface CustomerService {
    /**
     * 客户列表分页（名称模糊查询）
     * @param page
     * @param rows
     * @param customerName
     * @return
     */
    Map<String, Object> list(Integer page, Integer rows, String customerName);

    /**
     * 添加或者修改客户信息
     * @param customer
     * @return
     */
    ServiceVO save(Customer customer);

    /**
     * 删除客户（支持批量删除）
     * @param idList
     * @return
     */
    ServiceVO delete(String[] idList);
}
