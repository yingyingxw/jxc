package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.UnitDao;
import com.atguigu.jxc.entity.Unit;
import com.atguigu.jxc.service.UnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author CYY
 * @date 2023-07-04 18:58
 */
@Service
public class UnitServiceImpl implements UnitService {

    @Autowired
    private UnitDao unitDao;
    /**
     * 查询所有商品单位
     * @return
     */
    @Override
    public Map<String, Object> list() {
        //创建一个空的HashMap，用于存储查询结果。
        Map<String, Object> map = new HashMap<>();
        //查询结果返回一个List<Supplier>对象。
        List<Unit> unitList = unitDao.getUnitList();
        //将rows设置为查询得到的商品单位列表。
        map.put("rows",unitList);
        return map;
    }
}
