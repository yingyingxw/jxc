package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.GoodsTypeDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.service.GoodsTypeService;
import com.atguigu.jxc.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @description
 */
@Service
public class GoodsTypeServiceImpl implements GoodsTypeService {

    @Autowired
    private LogService logService;
    @Autowired
    private GoodsTypeDao goodsTypeDao;

    @Autowired
    private GoodsDao goodsDao;

    @Override
    public  ArrayList<Object> loadGoodsType() {
        logService.save(new Log(Log.SELECT_ACTION, "查询商品类别信息"));


        return this.getAllGoodsType(-1); // 根节点默认从-1开始
    }


    /**
     * 递归查询所有商品类别
     *
     * @return
     */
    public ArrayList<Object> getAllGoodsType(Integer parentId){

        ArrayList<Object> array = this.getGoodSTypeByParentId(parentId);

        for(int i = 0;i < array.size();i++){

            HashMap obj = (HashMap) array.get(i);

            if(obj.get("state").equals("open")){// 如果是叶子节点，不再递归

            }else{// 如果是根节点，继续递归查询
                obj.put("children", this.getAllGoodsType(Integer.parseInt(obj.get("id").toString())));
            }

        }

        return array;
    }

    /**
     * 根据父ID获取所有子商品类别
     * @return
     */
    public ArrayList<Object> getGoodSTypeByParentId(Integer parentId){

        ArrayList<Object> array = new ArrayList<>();

        List<GoodsType> goodsTypeList = goodsTypeDao.getAllGoodsTypeByParentId(parentId);

        System.out.println("goodsTypeList" + goodsTypeList);
        //遍历商品类别
        for(GoodsType goodsType : goodsTypeList){

            HashMap obj = new HashMap<String, Object>();

            obj.put("id", goodsType.getGoodsTypeId());
            obj.put("text", goodsType.getGoodsTypeName());

            if(goodsType.getGoodsTypeState() == 1){
                obj.put("state", "closed");

            }else{
                obj.put("state", "open");
            }

            obj.put("iconCls", "goods-type");

            HashMap<String, Object> attributes = new HashMap<>();
            attributes.put("state", goodsType.getGoodsTypeState());
            obj.put("attributes", attributes);

            array.add(obj);

        }

        return array;
    }

    /**
     * 新增分类
     * @param goodsTypeName 商品类别名称
     * @param pId   父商品类别id
     * @return
     */
    @Override
    public ServiceVO save(String goodsTypeName, Integer pId) {
        //新增商品分类
        goodsTypeDao.addGoodsType(goodsTypeName, pId);
        //修改商品分类的goodsTypeState为1---新增商品的状态为0，修改他的父节点的状态为1
        this.updateState(pId,1);
        //返回一个包含成功码和成功信息的ServiceVO对象
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    /**
     *修改商品分类的类别状态
     * 修改新增商品分类的父分类的类别状态为1
     * @param goodsTypeId
     * @param goodsTypeState
     */
    private void updateState(Integer goodsTypeId, Integer goodsTypeState) {
        goodsTypeDao.updateState(goodsTypeId,goodsTypeState);
    }

    /**
     * 删除分类
     * @param goodsTypeId
     * @return
     */
    @Override
    public ServiceVO delete(Integer goodsTypeId) {
        // 获取要删除的商品分类
        GoodsType goodsType = this.getGoodsTypeById(goodsTypeId);
        // 删除分类
        goodsTypeDao.deleteGoodsType(goodsTypeId);
        // 判断要删除分类删除后，其父分类是否有子分类
        Integer pId = goodsType.getPId();
        if (!haveOtherChild(pId)) {
            updateState(pId, 0);
        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }


    /**
     * 判断指定父分类是否存在其他子分类
     * @param pId 父分类ID
     * @return 是否存在其他子分类
     */
    private boolean haveOtherChild(Integer pId) {
        List<GoodsType> childList = goodsTypeDao.getAllGoodsTypeByParentId(pId);
        return childList !=null && childList.size()>0;
    }

    /**
     * 根据商品分类ID获取商品分类信息
     * @param goodsTypeId 商品分类ID
     * @return 商品分类信息
     */
    private GoodsType getGoodsTypeById(Integer goodsTypeId) {
        return goodsTypeDao.getGoodsTypeById(goodsTypeId);
    }
}
