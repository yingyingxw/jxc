package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.DamageListGoodsDao;
import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.UserDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.*;
import com.atguigu.jxc.service.DamageListGoodsService;
import com.atguigu.jxc.service.LogService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author CYY
 * @date 2023-07-05 17:55
 */
@Slf4j
@Service
public class DamageListGoodsServiceImpl implements DamageListGoodsService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private DamageListGoodsDao damageListGoodsDao;

    @Autowired
    private GoodsDao goodsDao;

    @Autowired
    private LogService logService;

    /**
     * 保存商品报损信息
     * @param damageList  商品报损单信息实体
     * @param damageListGoodsStr 商品报损单信息JSON字符串
     * @return
     */
    @Override
    public ServiceVO save(DamageList damageList, String damageListGoodsStr) {
        //使用谷歌的 Gson 将 JSON 字符串数组 damageListGoodsStr 转换为具体的集合类型 List<DamageListGoods>
        Gson gson = new Gson();
        List<DamageListGoods> damageListGoodsList =
                gson.fromJson(damageListGoodsStr, new TypeToken<List<DamageListGoods>>() {}.getType());
        //从数据库中查找当前操作用户，并为 damageList 设置用户id
        User currentUser = userDao.findUserByName((String) SecurityUtils.getSubject().getPrincipal());
        damageList.setUserId(currentUser.getUserId());
        //调用 damageListGoodsDao 的 saveDamageListGoods 方法保存商品报损单信息
        damageListGoodsDao.saveDamageList(damageList);
        //保存商品报损单商品信息
        //遍历 damageListGoodsList，对每个 DamageListGoods 对象执行以下操作
        for (DamageListGoods damageListGoods : damageListGoodsList) {
            //a. 设置 damageListId 为当前的报损单id
            damageListGoods.setDamageListId(damageList.getDamageListId());
            //b. 调用 damageGoodsDao 的 saveDamageListGoods 方法保存报损单商品信息
            damageListGoodsDao.saveDamageListGoods(damageListGoods);
            //c. 根据报损单商品的商品id从数据库中获取对应的商品信息
            Goods goods = goodsDao.findByGoodsId(damageListGoods.getGoodsId());
            //d. 更新商品的库存数量为当前库存减去报损数量
            goods.setInventoryQuantity(goods.getInventoryQuantity() - damageListGoods.getGoodsNum());
            //e. 调用 goodsDao 的 updateGoods 方法更新商品信息
            goodsDao.updateGoods(goods);
        }
        //调用 logService 的 save 方法保存日志信息
        logService.save(new Log(Log.INSERT_ACTION,"新增报损单:" + damageList.getDamageNumber()));
        //返回一个包含成功消息的 ServiceVO 对象
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    /**
     * 报损单查询
     * @param sTime  开始时间
     * @param eTime  结束时间
     * @return
     */
    @Override
    public Map<String, Object> list(String sTime, String eTime) {
        //1.创建一个空的HashMap，用于存储查询结果
        Map<String, Object> map = new HashMap<>();
        //2、根据传入的查询条件（sTime、eTime），调用 saleListGoodsDao 的 getDamageList 方法查询符合条件的报损单据信息
        List<DamageList> damageListList = damageListGoodsDao.getDamageList(sTime,eTime);
        //调用 logService 的 save 方法保存查询操作的日志信息，日志内容为 “报损单据查询”
        logService.save(new Log(Log.SELECT_ACTION,"报损单据查询"));
        //将查询结果 damageListList 存放在 rows 字段中
        map.put("rows",damageListList);
        //返回一个包含查询结果的 Map 对象 result，其中包含 rows 字段。
        return map;
    }

    /**
     * 查询报损单商品信息
     * @param damageListId  报损单Id
     * @return
     */
    @Override
    public Map<String, Object> goodsList(Integer damageListId) {
        //1.创建一个空的HashMap，用于存储查询结果
        Map<String, Object> map = new HashMap<>();
        //2、根据传入的报损单单号（damageListId），调用 damageListGoodsDao 的 getDamageListGoodsByDamageListId 方法查询该报损单对应的商品信息
        List<DamageListGoods> damageListGoodsList = damageListGoodsDao.getDamageListGoodsByDamageListId(damageListId);
        //调用 logService 的 save 方法保存查询操作的日志信息，日志内容为 “报损单商品信息查询”
        logService.save(new Log(Log.SELECT_ACTION,"报损单商品信息查询"));
        //将查询结果 damageListGoodsList 存放在 rows 字段中
        map.put("rows",damageListGoodsList);
        //返回一个包含查询结果的 Map 对象 map，包含 rows 字段
        return map;
    }
}
