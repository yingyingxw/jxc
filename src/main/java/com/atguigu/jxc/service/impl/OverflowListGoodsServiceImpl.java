package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.OverflowListGoodsDao;
import com.atguigu.jxc.dao.UserDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.*;
import com.atguigu.jxc.service.LogService;
import com.atguigu.jxc.service.OverflowListGoodsService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author CYY
 * @date 2023-07-05 19:52
 */
@Service
public class OverflowListGoodsServiceImpl implements OverflowListGoodsService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private OverflowListGoodsDao overflowListGoodsDao;

    @Autowired
    private GoodsDao goodsDao;

    @Autowired
    private LogService logService;

    /**
     * 新增报溢单
     * @param overflowList   商品报溢单实体
     * @param overflowListGoodsStr  商品报溢单信息JSON字符串
     * @return
     */
    @Override
    public ServiceVO save(OverflowList overflowList, String overflowListGoodsStr) {
        //使用谷歌的 Gson 将 JSON 字符串数组 overflowListGoodsStr 转换为具体的集合类型 List<OverflowListGoods>
        Gson gson = new Gson();
        List<OverflowListGoods> overflowListGoodsList =
                gson.fromJson(overflowListGoodsStr, new TypeToken<List<OverflowListGoods>>() {}.getType());
        //从数据库中查找当前操作用户，并为 overflowList 设置用户id
        User currentUser = userDao.findUserByName((String) SecurityUtils.getSubject().getPrincipal());
        overflowList.setUserId(currentUser.getUserId());
        //调用 overflowListGoodsDao 的 saveOverflowList 方法保存商品报溢单信息
        overflowListGoodsDao.saveOverflowList(overflowList);

        //保存商品报溢单商品信息
        //遍历 overflowListGoodsStr，对每个 OverflowListGoods 对象执行以下操作
        for (OverflowListGoods overflowListGoods : overflowListGoodsList) {
            //a. 设置 OverflowListId 为当前的报溢单id
            overflowListGoods.setOverflowListId(overflowList.getOverflowListId());
            //b. 调用 overflowListGoodsDao 的 saveOverflowListGoods 方法保存报溢单商品信息
            overflowListGoodsDao.saveOverflowListGoods(overflowListGoods);
            //c. 根据报溢单商品的商品id从数据库中获取对应的商品信息
            Goods goods = goodsDao.findByGoodsId(overflowListGoods.getGoodsId());
            //d. 更新商品的库存数量为当前库存加上报溢数量
            goods.setInventoryQuantity(goods.getInventoryQuantity() + overflowListGoods.getGoodsNum());
            //e. 调用 goodsDao 的 updateGoods 方法更新商品信息
            goodsDao.updateGoods(goods);
        }
        //调用 logService 的 save 方法保存日志信息
        logService.save(new Log(Log.INSERT_ACTION,"新增报溢单:" + overflowList.getOverflowNumber()));
        //返回一个包含成功消息的 ServiceVO 对象
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    /**
     * 报溢单查询
     * @param sTime  开始时间
     * @param eTime  结束时间
     * @return
     */
    @Override
    public Map<String, Object> list(String sTime, String eTime) {
        //1.创建一个空的HashMap，用于存储查询结果
        Map<String, Object> map = new HashMap<>();
        //2、根据传入的查询条件（sTime、eTime），调用 saleListGoodsDao 的 getOverflowList 方法查询符合条件的报损单据信息
        List<OverflowList> overflowListList = overflowListGoodsDao.getOverflowList(sTime,eTime);
        //调用 logService 的 save 方法保存查询操作的日志信息，日志内容为 “报溢单据查询”
        logService.save(new Log(Log.SELECT_ACTION,"报溢单据查询"));
        //将查询结果 overflowListList 存放在 rows 字段中
        map.put("rows",overflowListList);
        //返回一个包含查询结果的 Map 对象 result，其中包含 rows 字段。
        return map;
    }

    /**
     * 查询报溢单商品信息
     * @param overflowListId  报损单Id
     * @return
     */
    @Override
    public Map<String, Object> goodsList(Integer overflowListId) {
        //1.创建一个空的HashMap，用于存储查询结果
        Map<String, Object> map = new HashMap<>();
        //2、根据传入的报溢单单号（overflowListId），调用 overflowListGoodsDao 的 getOverflowListGoodsByOverflowListId 方法查询该报溢单对应的商品信息
        List<OverflowListGoods> overflowListGoodsList = overflowListGoodsDao.getOverflowListGoodsByOverflowListId(overflowListId);
        //调用 logService 的 save 方法保存查询操作的日志信息，日志内容为 “报溢单商品信息查询”
        logService.save(new Log(Log.SELECT_ACTION,"报溢单商品信息查询"));
        //将查询结果 overflowListGoodsList 存放在 rows 字段中
        map.put("rows",overflowListGoodsList);
        //返回一个包含查询结果的 Map 对象 map，包含 rows 字段
        return map;
    }
}

