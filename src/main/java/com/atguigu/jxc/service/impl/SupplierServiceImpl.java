package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.LogService;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author CYY
 * @date 2023-07-04 10:59
 */
@Service
@Transactional
public class SupplierServiceImpl implements SupplierService {
    @Autowired
    private SupplierDao supplierDao;

    @Autowired
    private LogService logService;
    /**
     * 分页查询供应商
     * @param page
     * @param rows
     * @param supplierName
     * @return
     */
    @Override
    public Map<String, Object> list(Integer page, Integer rows, String supplierName) {
        //首先创建一个空的HashMap，用于存储查询结果。
        HashMap<String, Object> map = new HashMap<>();
        //对传入的page进行处理，如果page为0，则将其设置为1。
        page = page == 0?1 : page;
        //根据计算得到的offSet（偏移量），调用supplierDao的getSupplierList方法查询供应商列表。
        int offSet = (page - 1)*rows;
        // getSupplierList方法接收三个参数：offSet、rows和supplierName，分别表示偏移量、每页显示数量和供应商名称。查询结果返回一个List<Supplier>对象。
        List<Supplier> supplierList = supplierDao.getSupplierList(offSet,rows,supplierName);
        //调用logService的save方法，保存一条日志记录，记录当前操作为"分页查询用户信息"。
        logService.save(new Log(Log.SELECT_ACTION,"分页查询供应商信息"));
        //将total设置为supplierDao的getSupplierCount方法的返回值，该方法用于查询符合条件的供应商总数。
        map.put("total",supplierDao.getSupplierCount(supplierName));
        //将rows设置为查询得到的供应商列表。
        map.put("rows",supplierList);
        //返回结果Map。
        return map;
    }

    /**
     * 添加或者修改供应商信息
     * @param supplier 供应商信息实体
     * @return
     */
    @Override
    public ServiceVO save(Supplier supplier) {
        //首先判断Supplier对象的supplierId属性是否为空，如果为空，则说明是新增操作。
        if (supplier.getSupplierId() == null){
            /*//在新增操作中，调用supplierDao的getSupplierByName方法，根据供应商名查询供应商是否已存在。
            Supplier supplierByName = supplierDao.getSupplierByName(supplier.getSupplierName());
            if (supplierByName !=null){
                //如果查询到已存在的供应商，返回一个包含错误码和错误信息的ServiceVO对象，表示供应商已存在。
                return new ServiceVO(ErrorCode.ACCOUNT_EXIST_CODE,ErrorCode.ACCOUNT_EXIST_MESS);
            }*/
            //如果查询到不存在的供应商，执行插入操作，调用supplierDao的addSupplier方法，将Supplier对象插入数据库。
            supplierDao.addSupplier(supplier);

            //调用logService的save方法，保存一条日志记录，记录当前操作为"添加供应商:供应商名"。
            logService.save(new Log(Log.INSERT_ACTION,"添加供应商:" + supplier.getSupplierName()));

        }else {
            //在更新操作中，调用supplierDao的updateSupplier方法，更新数据库中对应的供应商信息。
            supplierDao.updateSupplier(supplier);
            //调用logService的save方法，保存一条日志记录，记录当前操作为"修改供应商:供应商名"。
            logService.save(new Log(Log.UPDATE_ACTION,"修改供应商:" + supplier.getSupplierName()));

        }
        //最后，无论是新增操作还是更新操作，都返回一个包含成功码和成功信息的ServiceVO对象。
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    /**
     * 删除供应商（支持批量删除）
     * @param idList
     * @return
     */
    @Override
    public ServiceVO delete(String[] idList) {
        supplierDao.deleteSupplier(idList);
        //返回一个包含成功码和成功信息的ServiceVO对象。
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }
}
