package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.service.CustomerReturnListGoodsService;
import com.atguigu.jxc.service.GoodsService;
import com.atguigu.jxc.service.LogService;
import com.atguigu.jxc.service.SaleListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description
 */
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;

    @Autowired
    private SaleListGoodsService saleListGoodsService;

    @Autowired
    private CustomerReturnListGoodsService customerReturnListGoodsService;

    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();

        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for(int i = 4;i > intCode.toString().length();i--){

            unitCode = "0"+unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }


    /**
     * 分页查询商品信息（可以根据分类、名称查询）
     * @param page 当前页
     * @param rows 每页显示条数
     * @param goodsName 商品名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @Override
    public Map<String, Object> list(Integer page, Integer rows, String goodsName, Integer goodsTypeId) {
        //1.创建一个空的HashMap，用于存储查询结果
        Map<String, Object> map = new HashMap<>();
        //2.对传入的page进行处理，如果page为0，将其设置为1
        page= page == 0?1 : page;
        //3.根据计算得到的offSet，调用goodsDao的getGoodsList方法查询商品信息列表
        int offSet = (page - 1)*rows;
        //4.getGoodsList方法接收三个参数：offSet、rows、goodsName和goodsTypeId，分别表示偏移量、每页显示数量和商品名称，商品分类id。查询结果返回一个List<Goods>对象。
        List<Goods> goodsList = goodsDao.getGoodsList(offSet,rows,goodsName,goodsTypeId);
        //5.将total设置为goodsDao的getGoodsCount方法的返回值，该方法用于查询符合条件的商品总数。
        map.put("total",goodsDao.getGoodsCount(goodsName,goodsTypeId));
        //6.将rows设置为查询得到的商品列表。
        map.put("rows",goodsList);
        //7.返回结果Map
        return map;
    }

    /**
     * 添加或修改商品信息
     * @param goods 商品信息实体
     * @return
     */
    @Override
    public ServiceVO save(Goods goods) {
        //首先判断Goods对象的goodsId属性是否为空，如果为空，则说明是新增操作。
        if (goods.getGoodsId() == null){

            //添加库存数量
            goods.setInventoryQuantity(0);
            //添加库存state
            goods.setState(0);
            //如果查询到不存在的商品，执行插入操作，调用goodsDao的addGoods方法，将Goods对象插入数据库。
            goodsDao.addGoods(goods);
        }else {
            //在更新操作中，调用goodsDao的updateGoods方法，更新数据库中对应的供应商信息。
            goodsDao.updateGoods(goods);
        }
        //最后，无论是新增操作还是更新操作，都返回一个包含成功码和成功信息的ServiceVO对象。
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    /**
     * 删除商品信息
     * (要判断商品状态,入库、有进货和销售单据的不能删除)
     * @param goodsId 商品ID
     * @return
     */
    @Override
    public ServiceVO delete(Integer goodsId) {

        //根据商品Id查询商品state   0表示初始值,1表示已入库，2表示有进货或销售单据
        Goods goods = goodsDao.getStateByGoodsId(goodsId);
        //判断商品状态,有入库--1、有进货和销售单据--2 的不能删除
        if (goods.getState() == 0){
            //删除商品信息
            goodsDao.deleteGoods(goodsId);
            //返回一个包含成功码和成功信息的ServiceVO对象。100 请求成功
            return new ServiceVO<>(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);

        } else if (goods.getState() == 1){
            //401 该商品已入库，不能删除
            return new ServiceVO<>(ErrorCode.STORED_ERROR_CODE,ErrorCode.STORED_ERROR_MESS);
        }else {
            // 402  该商品有进货或销售单据，不能删除
            return new ServiceVO<>(ErrorCode.HAS_FORM_ERROR_CODE,ErrorCode.HAS_FORM_ERROR_MESS);
        }
    }

    /**
     * 分页查询商品库存信息
     * 查询当前库存（可根据商品类别、商品编码或名称搜索）
     * @param page 当前页
     * @param rows 每页显示条数
     * @param codeOrName 商品编码或名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @Override
    public Map<String, Object> listInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {

        //1.创建一个空的HashMap，用于存储查询结果
        Map<String, Object> map = new HashMap<>();
        //2.对传入的page进行处理，如果page为0，将其设置为1
        page= page == 0?1 : page;
        //3.根据计算得到的offSet，调用goodsDao的getGoodsList方法查询商品信息列表
        int offSet = (page - 1)*rows;
        //4.getGoodsList方法接收三个参数：offSet、rows、goodsName和goodsTypeId，分别表示偏移量、每页显示数量和商品名称，商品分类id。查询结果返回一个List<Goods>对象。
        List<Goods> goodsList = goodsDao.getListInventory(offSet,rows,codeOrName,goodsTypeId);
        for (Goods goods : goodsList) {
            //销售总数写死为0
            //goods.setSaleTotal(0);

            Integer goodsId = goods.getGoodsId();
            //销售总数 = 商品销售数量 - 客户退货数量
            Integer goodsSaleNum = saleListGoodsService.getSaleTotalByGoodsId(goodsId);
            Integer goodsReturnNum = customerReturnListGoodsService.getCustomerReturnTotalByGoodsId(goodsId);
            goods.setSaleTotal(goodsSaleNum - goodsReturnNum);
        }
        //5.将total设置为goodsDao的getGoodsCount方法的返回值，该方法用于查询符合条件的商品总数。
       // map.put("total",goodsDao.getGoodsCount(codeOrName,goodsTypeId));
        map.put("total", goodsList.size());
        //6.将rows设置为查询得到的商品列表。
        map.put("rows",goodsList);
        //7.返回结果Map
        return map;
    }

    /**
     * 分页查询无库存商品信息
     * 无库存商品列表展示（可以根据商品名称或编码查询）
     * @param page 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    @Override
    public Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        //1.创建一个空的HashMap，用于存储查询结果
        Map<String, Object> map = new HashMap<>();
        //2.对传入的page进行处理，如果page为0，将其设置为1
        page= page == 0?1 : page;
        //3.根据计算得到的offSet，调用goodsDao的getGoodsList方法查询商品信息列表
        int offSet = (page - 1)*rows;
        //4.查询结果返回一个List<Goods>对象。
        List<Goods> goodsList = goodsDao.getNoInventoryQuantity(offSet,rows,nameOrCode);
        for (Goods goods : goodsList) {
            Integer goodsTypeId = goods.getGoodsTypeId();
            String goodsTypeName = goodsDao.getGoodsTypeNameBygoodsTypeId(goodsTypeId);
            goods.setGoodsTypeName(goodsTypeName);
        }
        //5.将total设置为goodsDao的getGoodsCount方法的返回值，该方法用于查询符合条件的商品总数。
        map.put("total",goodsList.size());
        //6.将rows设置为查询得到的商品列表。
        map.put("rows",goodsList);
        //7.返回结果Map
        return map;
    }

    /**
     * 分页查询有库存商品信息
     * 有库存商品列表展示（可以根据商品名称或编码查询）
     * @param page 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    @Override
    public Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        //1.创建一个空的HashMap，用于存储查询结果
        Map<String, Object> map = new HashMap<>();
        //2.对传入的page进行处理，如果page为0，将其设置为1
        page= page == 0?1 : page;
        //3.根据计算得到的offSet，调用goodsDao的getGoodsList方法查询商品信息列表
        int offSet = (page - 1)*rows;
        //4.查询结果返回一个List<Goods>对象。
        List<Goods> goodsList = goodsDao.getHasInventoryQuantity(offSet,rows,nameOrCode);
        for (Goods goods : goodsList) {
            Integer goodsTypeId = goods.getGoodsTypeId();
            String goodsTypeName = goodsDao.getGoodsTypeNameBygoodsTypeId(goodsTypeId);
            goods.setGoodsTypeName(goodsTypeName);
        }
        //5.将total设置为goodsDao的getGoodsCount方法的返回值，该方法用于查询符合条件的商品总数。
        map.put("total",goodsList.size());
        //6.将rows设置为查询得到的商品列表。
        map.put("rows",goodsList);
        //7.返回结果Map
        return map;
    }

    /**
     * 添加商品期初库存
     * @param goodsId 商品ID
     * @param inventoryQuantity 库存
     * @param purchasingPrice 成本价
     * @return
     */
    @Override
    public ServiceVO saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice) {
        //判断Goods对象的inventory_quantity 是否小于等于0，如果是，说明无库存，需要添加库存--添加期初数量和成本价(本质也是修改)
        if (inventoryQuantity == 0){
            goodsDao.updateInventoryQuantityAndPurchasingPrice(goodsId,purchasingPrice);
        }else {
            //如果大于0 ,则说明有库存，可以进行修改数量或成本价
            goodsDao.updateInventoryQuantityOrPurchasingPrice(goodsId,inventoryQuantity,purchasingPrice);
        }
        //返回一个包含成功码和成功信息的ServiceVO对象。100 请求成功
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    /**
     * 删除商品库存
     * （要判断商品状态 入库、有进货和销售单据的不能删除）
     * @param goodsId 商品ID
     * @return
     */
    @Override
    public ServiceVO deleteStock(Integer goodsId) {
        //根据商品Id查询商品state   0表示初始值,1表示已入库，2表示有进货或销售单据
        Goods goods = goodsDao.getStateByGoodsId(goodsId);
        //判断商品状态,有入库--1、有进货和销售单据--2 的不能删除
        if (goods.getState() == 0){
            //删除商品信息
            goodsDao.deleteGoods(goodsId);
            //返回一个包含成功码和成功信息的ServiceVO对象。100 请求成功
            return new ServiceVO<>(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);

        } else if (goods.getState() == 1){
            //401 该商品已入库，不能删除
            return new ServiceVO<>(ErrorCode.STORED_ERROR_CODE,ErrorCode.STORED_ERROR_MESS);
        }else {
            // 402  该商品有进货或销售单据，不能删除
            return new ServiceVO<>(ErrorCode.HAS_FORM_ERROR_CODE,ErrorCode.HAS_FORM_ERROR_MESS);
        }
    }

    /**
     * 查询库存报警商品信息
     *查询所有 当前库存量 小于 库存下限的商品信息
     * @return
     */
    @Override
    public Map<String, Object> listAlarm() {
        //1.创建一个空的HashMap，用于存储查询结果
        Map<String, Object> map = new HashMap<>();
        //查询所有 当前库存量 小于 库存下限的商品信息
        List<Goods> goodsList = goodsDao.listAlarm();
        //2.将rows设置为查询得到的商品列表。
        map.put("rows",goodsList);
        //3.返回结果Map
        return map;
    }
}
