package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.CustomerService;
import com.atguigu.jxc.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author CYY
 * @date 2023-07-04 15:36
 */
@Service
public class CustomerServiceImpl implements CustomerService {
        @Autowired
        private CustomerDao customerDao;

        @Autowired
        private LogService logService;
    /**
     *分页查询客户列表（名称模糊查询）
     * @param page
     * @param rows
     * @param customerName
     * @return
     */
    @Override
    public Map<String, Object> list(Integer page, Integer rows, String customerName) {

        //1、创建一个空的HashMap，用于存储查询结果。
        HashMap<String, Object> map = new HashMap<>();
        //2、对传入的page进行处理，如果page为0，则将其设置为1。
        page = page == 0?1 : page;
        //3、根据计算得到的offSet（偏移量），调用customerDao的getCustomerList方法查询供客户列表。
        int offSet = (page - 1)*rows;
        // 4、getCustomerList方法接收三个参数：offSet、rows和customerName，分别表示偏移量、每页显示数量和客户名称。查询结果返回一个List<Customer>对象。
       List<Customer> customerList = customerDao.getCustomerList(offSet,rows,customerName);

        //5、调用logService的save方法，保存一条日志记录，记录当前操作为"分页查询用户信息"。
        logService.save(new Log(Log.SELECT_ACTION,"分页查询客户信息"));

        //6、将total设置为customerDao的getCustomerCount方法的返回值，用于查询符合条件的客户总数。
        map.put("total",customerDao.getCustomerCount(customerName));
        //7、将rows设置为查询得到的客户列表。
        map.put("rows",customerList);
        //8、返回结果Map。
        return map;
    }

    /**
     * 添加或者修改客户信息
     * @param customer
     * @return
     */
    @Override
    public ServiceVO save(Customer customer) {
        //判断Customer对象的customerId属性是否为空
        // 如果为空，则说明是新增操作。
        if (customer.getCustomerId() == null){
           /* //在新增操作中，调用customerDao的getCustomerByName方法，根据客户名查询客户是否已存在。
            Customer customerByName = customerDao.getCustomerByName(customer.getCustomerName());
            if (customerByName !=null){
                //如果查询到已存在的客户，返回一个包含错误码和错误信息的ServiceVO对象，表示客户已存在。
                return new ServiceVO(ErrorCode.ACCOUNT_EXIST_CODE,ErrorCode.ACCOUNT_EXIST_MESS);
            }*/
            //如果查询到不存在的客户，执行插入操作，调用customerDao的addCustomer方法，将Customer对象插入数据库。
            customerDao.addCustomer(customer);

            //调用logService的save方法，保存一条日志记录，记录当前操作为"添加客户:客户"。
            logService.save(new Log(Log.INSERT_ACTION,"添加客户:" + customer.getCustomerName()));

        }else {
            //在更新操作中，调用customerDao的updateCustomer方法，更新数据库中对应的客户信息。
            customerDao.updateCustomer(customer);
            //调用logService的save方法，保存一条日志记录，记录当前操作为"修改客户:客户名"。
            logService.save(new Log(Log.UPDATE_ACTION,"修改客户:" + customer.getCustomerName()));

        }
        //最后，无论是新增操作还是更新操作，都返回一个包含成功码和成功信息的ServiceVO对象。
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    /**
     * 删除客户（支持批量删除）
     * @param idList
     * @return
     */
    @Override
    public ServiceVO delete(String[] idList) {
        customerDao.deleteSupplier(idList);
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }
}
