package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author CYY
 * @date 2023-07-04 11:25
 */
public interface SupplierDao {
    /**
     * 根据供应商名称模糊分页查询供应商列表
     * @param offSet
     * @param pageRow
     * @param supplierName
     * @return
     */
    List<Supplier> getSupplierList(@Param("offSet") Integer offSet, @Param("pageRow") Integer pageRow,@Param("supplierName") String supplierName);

    /**
     * 根据供应商名称模糊查询供应商列表的数量
     * @param supplierName
     * @return
     */
    Integer getSupplierCount(@Param("supplierName") String supplierName);

    /**
     *根据供应商名查找供应商
     * @param supplierName
     */
    Supplier getSupplierByName(String supplierName);

    /**
     * 添加供应商
     * @param supplier
     */
    Integer addSupplier(Supplier supplier);

    /**
     * 修改供应商
     * @param supplier
     */
    Integer updateSupplier(Supplier supplier);

    /**
     * 根据供应商Id查询供应商
     * @param supplierId
     * @return
     */
    Supplier getSupplierById(String supplierId);

    /**
     * 删除供应商
     * @param idList
     * @return
     */
    Integer deleteSupplier(@Param("idList") String[] idList);
}
