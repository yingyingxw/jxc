package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;

import java.util.List;

/**
 * @author CYY
 * @date 2023-07-05 18:00
 */
public interface DamageListGoodsDao {

    /**
     * 保存商品报损单信息
     * @param damageList
     */
    Integer saveDamageList(DamageList damageList);

    /**
     * 保存报损单商品信息
     * @param damageListGoods
     */
    Integer saveDamageListGoods(DamageListGoods damageListGoods);

    /**
     * 报损单查询
     * @param sTime
     * @param eTime
     * @return
     */
    List<DamageList> getDamageList(String sTime, String eTime);

    /**
     * 查询报损单商品信息
     * @param damageListId
     * @return
     */
    List<DamageListGoods> getDamageListGoodsByDamageListId(Integer damageListId);
}
