package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.GoodsType;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @description 商品类别
 */
@Repository
public interface GoodsTypeDao {

    List<GoodsType> getAllGoodsTypeByParentId(Integer pId);

    Integer updateGoodsTypeState(GoodsType parentGoodsType);


    /**
     * 新增分类
     * @param goodsTypeName
     * @param pId
     * @return
     */
    Integer addGoodsType(String goodsTypeName, Integer pId);

    /**
     * 修改分类类别状态
     * @param goodsTypeId
     * @param goodsTypeState
     * @return
     */
    Integer updateState(Integer goodsTypeId, Integer goodsTypeState);

    /**
     * 删除分类
     * @param goodsTypeId
     * @return
     */
    Integer deleteGoodsType(Integer goodsTypeId);


    /**
     * 根据商品分类ID获取商品分类信息
     * @param goodsTypeId
     * @return
     */
    GoodsType getGoodsTypeById(Integer goodsTypeId);

}
