package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @description 商品信息
 */
@Repository
public interface GoodsDao {


    String getMaxCode();


    /**
     * 根据商品名称或者商品分类Id模糊分页查询商品信息列表
     * @param offSet
     * @param rows
     * @param goodsName
     * @param goodsTypeId
     * @return
     */
    List<Goods> getGoodsList(Integer offSet, Integer rows, String goodsName, Integer goodsTypeId);

    /**
     *根据商品名称和商品分类Id模糊查询商品列表的数量
     * @param goodsName
     * @param goodsTypeId
     * @return
     */
    Integer getGoodsCount(String goodsName, Integer goodsTypeId);

    /**
     * 新增商品信息
     * @param goods
     */
    Integer addGoods(Goods goods);

    /**
     * 修改商品信息
     * @param goods
     */
    Integer updateGoods(Goods goods);

    /**
     * 根据商品Id查询商品state   0表示初始值,1表示已入库，2表示有进货或销售单据
     * @param goodsId
     * @return
     */
    Goods getStateByGoodsId(Integer goodsId);

    /**
     * 根据商品Id删除商品信息
     * @param goodsId
     * @return
     */
    Integer deleteGoods(Integer goodsId);


    /**
     * 分页查询商品库存信息
     * 查询当前库存（可根据商品类别、商品编码或名称搜索）
     * @param offSet 当前页
     * @param rows 每页显示条数
     * @param codeOrName 商品编码或名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    List<Goods> getListInventory(Integer offSet, Integer rows, String codeOrName, Integer goodsTypeId);


    /**
     * 分页查询无库存商品信息
     * 无库存商品列表展示（可以根据商品名称或编码查询）
     * @param offSet 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    List<Goods> getNoInventoryQuantity(Integer offSet, Integer rows, String nameOrCode);

    /**
     * 分页查询有库存商品信息
     * 有库存商品列表展示（可以根据商品名称或编码查询）
     * @param offSet 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    List<Goods> getHasInventoryQuantity(Integer offSet, Integer rows, String nameOrCode);


    /**
     * 根据商品id从数据库中获取对应的商品信息
     * @param goodsId
     * @return
     */
    Goods findByGoodsId(Integer goodsId);


    /**
     * 根据goodsTypeId获取商品分类名
     * @param goodsTypeId
     * @return
     */
    String getGoodsTypeNameBygoodsTypeId(Integer goodsTypeId);

    /**
     * 无库存，需要添加库存--添加期初数量和成本价
     * @param goodsId
     * @param purchasingPrice
     */
    void updateInventoryQuantityAndPurchasingPrice(Integer goodsId, double purchasingPrice);


    /**
     * 有库存，可以进行修改数量或成本价
     * @param goodsId
     * @param inventoryQuantity
     * @param purchasingPrice
     */
    void updateInventoryQuantityOrPurchasingPrice(Integer goodsId, Integer inventoryQuantity, double purchasingPrice);


    /**
     * 查询库存报警商品信息
     *查询所有 当前库存量 小于 库存下限的商品信息
     * @return
     */
    List<Goods> listAlarm();


}
