package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author CYY
 * @date 2023-07-05 19:51
 */
@Repository
public interface OverflowListGoodsDao {
    /**
     * 保存商品报溢单信息
     * @param overflowList
     * @return
     */
    Integer saveOverflowList(OverflowList overflowList);


    /**
     * 保存报溢单商品信息
     * @param overflowListGoods
     * @return
     */
    Integer saveOverflowListGoods(OverflowListGoods overflowListGoods);

    /**
     * 报溢单查询
     * @param sTime  开始时间
     * @param eTime  结束时间
     * @return
     */
    List<OverflowList> getOverflowList(String sTime, String eTime);

    /**
     * 查询报溢单商品信息
     * @param overflowListId
     * @return
     */
    List<OverflowListGoods> getOverflowListGoodsByOverflowListId(Integer overflowListId);
}
