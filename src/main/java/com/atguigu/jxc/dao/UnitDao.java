package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Unit;

import java.util.List;
import java.util.Map;

/**
 * @author CYY
 * @date 2023-07-04 18:58
 */
public interface UnitDao {
    /**
     * 查询所有商品单位
     * @return
     */
    List<Unit> getUnitList();

}
