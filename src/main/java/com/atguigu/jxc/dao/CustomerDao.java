package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author CYY
 * @date 2023-07-04 15:35
 */
public interface CustomerDao {
    /**
     * 根据客户名称模糊分页查询客户列表
     * @param offSet
     * @param pageRow
     * @param customerName
     * @return
     */
    List<Customer> getCustomerList(@Param("offSet") Integer offSet,@Param("pageRow") Integer pageRow,@Param("customerName") String customerName);

    /**
     * 根据客户名称查询客户列表的数量
     * @param customerName
     * @return
     */
    Integer getCustomerCount(@Param("customerName") String customerName);

    /**
     * 根据客户名查找客户
     * @param customerName
     * @return
     */
    Customer getCustomerByName(String customerName);

    /**
     * 新增客户
     * @param customer
     */
    Integer addCustomer(Customer customer);

    /**
     * 修改客户
     * @param customer
     */
    Integer updateCustomer(Customer customer);

    /**
     * 删除客户（支持批量删除）
     * @param idList
     */
    Integer deleteSupplier(@Param("idList") String[] idList);
}
